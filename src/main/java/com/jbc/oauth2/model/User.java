package com.jbc.oauth2.model;

public class User {
	public String idUser;
	public String password;
	public Integer idUserType;
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getIdUserType() {
		return idUserType;
	}
	public void setIdUserType(Integer idUserType) {
		this.idUserType = idUserType;
	}

	
}
