package com.jbc.oauth2.util;

import java.sql.Date;
import java.time.LocalDate;

public class UtilDao {
	
	public UtilDao() {
		throw new IllegalStateException("Clase de utilidad");
	}
	
	public static String generateQuery(String procName, int countParameter) {
		StringBuilder builder = new StringBuilder();
		int i;
		for (i = 0; i < countParameter; i++) {
			builder.append("?");
			if (i != (countParameter - 1)) {
				builder.append(",");
			}
		}

		return String.format("SELECT * FROM %s (%s);", procName, builder.toString());
	}
	
	public static Character getChar(String column) {
		Character character=null;
		if(column!=null) {
			character=column.charAt(0);
		}
		return character;
	}

	public static LocalDate getLocalDate(Date date) {
		LocalDate localDate=null;
		if(date!=null) {
			localDate=date.toLocalDate();
		}
		return localDate;
	}
	
	public static <T> T coalesce(@SuppressWarnings("unchecked") T ...items) {
	    for(T i : items) {
	    	if(i != null) {
	    		return i;
	    	}
	    } 
	    return null;
	}
	
}
