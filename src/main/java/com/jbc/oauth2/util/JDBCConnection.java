package com.jbc.oauth2.util;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.jbc.oauth2.EnvironmentProperties;


public class JDBCConnection {
	public DataSource getDataSourceAuxiliar() throws IOException
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();

		Properties prop = EnvironmentProperties.getJDBCProperties();
	   	
		dataSource.setDriverClassName(prop.getProperty("JDBC_DRIVER"));
		dataSource.setUrl(prop.getProperty("JDBC_URL"));
		dataSource.setUsername(prop.getProperty("JDBC_USERNAME"));
		dataSource.setPassword(prop.getProperty("JDBC_PASSWORD"));
		return dataSource;
	}
}
