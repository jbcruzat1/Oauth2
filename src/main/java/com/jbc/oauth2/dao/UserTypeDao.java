package com.jbc.oauth2.dao;

import java.io.IOException;

import com.jbc.oauth2.model.UserType;



public interface UserTypeDao {
	UserType findByIdUserType(Integer id) throws IOException ;
}
