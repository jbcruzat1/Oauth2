package com.jbc.oauth2.dao;

import java.io.IOException;
import java.util.List;

import com.jbc.oauth2.model.User;



public interface UserDao {
	User 		loginUser(String idUser, String password) throws IOException;
	User			findUserById(String idUser) throws IOException;
}
