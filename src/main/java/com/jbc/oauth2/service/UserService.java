package com.jbc.oauth2.service;
import java.io.IOException;

import com.jbc.oauth2.model.User;


public interface UserService {
	User findUserById(String idUser) throws IOException; 
	User loginUser(String idUser, String password) throws IOException;

}
