package com.jbc.oauth2;

import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.jbc.oauth2.dao.UserDao;
import com.jbc.oauth2.dao.UserTypeDao;
import com.jbc.oauth2.daoimpl.UserDaoImpl;
import com.jbc.oauth2.daoimpl.UserTypeDaoImpl;
import com.jbc.oauth2.model.User;
import com.jbc.oauth2.model.UserType;

import java.io.IOException;


import java.util.HashSet;
import java.util.Set;

@Configuration
@Service("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
	private UserDao userDAOImpl;
	private UserTypeDao userTypeDaoImpl;


	public CustomUserDetailsService() {
		this.userDAOImpl = new UserDaoImpl();
		this.userTypeDaoImpl = new UserTypeDaoImpl();
	}


	@Override
	public UserDetails loadUserByUsername(String idUser) throws UsernameNotFoundException {
		//System.out.println("entro loadUserByUsername");
		try {
			User user = userDAOImpl.findUserById(idUser);
			
			//System.out.println("user: " + user.getIdUser());
			//System.out.println("pass: " + user.getPassword());
			if (user.getIdUser() == null || user.getPassword() == null) {
				throw new UsernameNotFoundException("User not found");
			}
			return new org.springframework.security.core.userdetails.User(user.getIdUser(), passwordEncoder().encode(user.getPassword()),
			getAuthorities(user));

		} catch (Exception e) {
			throw new UsernameNotFoundException(e.getMessage());
		}
	}

	
	
	private Set<GrantedAuthority> getAuthorities(User user) throws IOException {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		UserType userType = userTypeDaoImpl.findByIdUserType(user.getIdUserType());
		GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(userType.getName());
		authorities.add(grantedAuthority);
		return authorities;
	}
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new  BCryptPasswordEncoder();
    }

}
