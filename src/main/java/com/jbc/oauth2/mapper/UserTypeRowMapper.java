package com.jbc.oauth2.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.jbc.oauth2.model.UserType;



public class UserTypeRowMapper implements RowMapper<UserType>
{
	public UserType mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserType userType = new UserType();
		userType.setId(rs.getInt("id"));
		userType.setName(rs.getString("name"));
		return userType;
	}
	
}