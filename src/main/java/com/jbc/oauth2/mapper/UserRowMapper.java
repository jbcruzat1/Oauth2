package com.jbc.oauth2.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.jbc.oauth2.model.User;



public class UserRowMapper implements RowMapper<User> {
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();

		user.setIdUser(rs.getString("iduser"));
		user.setPassword(rs.getString("password"));
		user.setIdUserType(rs.getInt("idusertype"));
		
		
		return user;
	}

}