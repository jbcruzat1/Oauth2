package com.jbc.oauth2;

import java.util.Properties;

public class EnvironmentProperties {

	public EnvironmentProperties() {

	}

	public static Properties getJDBCProperties() {
		final Properties prop = new Properties();
		
		final String JDBC_URL = "jdbc:postgresql://localhost:5432/postgres";
		final String JDBC_USERNAME = "postgres";
		final String JDBC_PASSWORD = "admin";

		prop.setProperty("AUTH_SERVER", "http://localhost:8081/Oauth2/oauth/token");
		prop.setProperty("JDBC_DRIVER", "org.postgresql.Driver");
		prop.setProperty("JDBC_URL", JDBC_URL);
		prop.setProperty("JDBC_USERNAME", JDBC_USERNAME);
		prop.setProperty("JDBC_PASSWORD", JDBC_PASSWORD);

		return prop;
	}
}
