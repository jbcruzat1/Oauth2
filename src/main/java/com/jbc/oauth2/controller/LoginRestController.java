package com.jbc.oauth2.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.jbc.oauth2.EnvironmentProperties;
import com.jbc.oauth2.model.AuthTokenInfo;
import com.jbc.oauth2.model.User;
import com.jbc.oauth2.service.UserService;
import com.jbc.oauth2.util.UtilEncript;


@RestController

public class LoginRestController {
	
	@Autowired 
	UserService userService; 
	
	private static HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		return headers;
	}

	private static HttpHeaders getHeadersWithClientCredentials() {
		String plainClientCredentials = "myclient:secret";
		String base64ClientCredentials = new String(Base64.encodeBase64(plainClientCredentials.getBytes()));
		HttpHeaders headers = getHeaders();
		headers.add("Authorization", "Basic " + base64ClientCredentials);
		return headers;
	}

	@SuppressWarnings("unchecked")
	public static AuthTokenInfo sendTokenRequest(String username, String passEncripted) throws IOException {
		
	   	
		AuthTokenInfo 					tokenInfo	= null;
		HttpEntity<String> 				request		= null;
		ResponseEntity<Object> 			response	= null;
		LinkedHashMap<String, Object> 	map;
		
		//PETICION SERVIDOR AUTENTICACION
		
		RestTemplate restTemplate = new RestTemplate();
		String QPM_PASSWORD = "?grant_type=password&username=[username]&password=[pass]".replace("[username]", username)
				.replace("[pass]", passEncripted);
		
		
		Properties prop = EnvironmentProperties.getJDBCProperties();
		String urlOauth = prop.getProperty("AUTH_SERVER");
		
		//System.out.println("AUTH_SERVER" + urlOauth);
		//System.out.println("QPM_PASSWORD" + QPM_PASSWORD);
		
		try {
			request = new HttpEntity<String>(getHeadersWithClientCredentials());
			response = restTemplate.exchange(	urlOauth + QPM_PASSWORD,
												HttpMethod.POST,
												request,
												Object.class);
			map = (LinkedHashMap<String, Object>) response.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			map = null;
		}
		if (map != null) {
			tokenInfo = new AuthTokenInfo();
			tokenInfo.setAccess_token((String) map.get("access_token"));
			tokenInfo.setToken_type((String) map.get("token_type"));
			tokenInfo.setRefresh_token((String) map.get("refresh_token"));
			tokenInfo.setExpires_in((int) map.get("expires_in"));
			tokenInfo.setScope((String) map.get("scope"));
		}
		return tokenInfo;
	}


	@RequestMapping(value = "/Login/Token", method = RequestMethod.POST)
	public ResponseEntity<Object> getUser(@RequestBody User user,
			UriComponentsBuilder ucBuilder) throws IOException {
		AuthTokenInfo tokenInfo = null;
		if(user.getIdUser() == null || user.getIdUser().equals("")
				|| user.getPassword() == null || user.getPassword().equals("")) {
			return new ResponseEntity<>("User and Password are required",HttpStatus.OK);
		}
		String passwordEncripted = this.encriptPassword(user.getPassword());
		tokenInfo = sendTokenRequest(user.getIdUser(), passwordEncripted);
		if (tokenInfo == null) {
			return new ResponseEntity<>("User not valid",HttpStatus.BAD_GATEWAY);
		}
		try {
			User userfound = userService.loginUser(user.getIdUser(),passwordEncripted);
			if (userfound != null) {
				return new ResponseEntity<>(tokenInfo, HttpStatus.OK);
			} else {
				return new ResponseEntity<>("User not found",HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>("Internal Error.",HttpStatus.OK);
		}
	}
	
	public String encriptPassword(String password) {
		MessageDigest digest;
		String encodedHashPass=null;
		try {
			digest 			= MessageDigest.getInstance("SHA-256");
			encodedHashPass = "\\x"+UtilEncript.bytesToHex(digest.digest(password.getBytes(StandardCharsets.UTF_8)));
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		}
		return encodedHashPass;
	}

}
