package com.jbc.oauth2.serviceimpl;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jbc.oauth2.dao.UserDao;
import com.jbc.oauth2.dao.UserTypeDao;
import com.jbc.oauth2.model.User;
import com.jbc.oauth2.service.UserService;



@Service("usuarioService")
public class UserServiceImpl implements UserService{
	
	@Autowired
	UserDao userDao;
	@Autowired
	UserTypeDao userTypeDao;

	@Override
	public User findUserById(String idUser) throws IOException {
		return userDao.findUserById(idUser);
	}
	
	@Override
	public User loginUser(String idUser, String password) throws IOException {
		return userDao.loginUser(idUser, password);
	}
	
	
}
