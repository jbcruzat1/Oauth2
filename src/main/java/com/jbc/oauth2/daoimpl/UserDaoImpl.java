package com.jbc.oauth2.daoimpl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.jbc.oauth2.dao.UserDao;
import com.jbc.oauth2.mapper.UserRowMapper;
import com.jbc.oauth2.model.User;
import com.jbc.oauth2.util.JDBCConnection;
import com.jbc.oauth2.util.UtilDao;



@Repository
public class UserDaoImpl implements UserDao {

	JdbcTemplate jdbcTemplate = new JdbcTemplate();
	
	private Properties config = new Properties();
	
	private static final String FIND_BY_ID;
	private static final String LOGIN_USER;
	
	static {
		FIND_BY_ID 					= UtilDao.generateQuery("findByIdUser", 1);
		LOGIN_USER				= UtilDao.generateQuery("loginUser",2);
	}

	
	@Override
	public User findUserById(String idUser) {
		User retornedObj = null;
		Object[] params = { idUser };
		
		
		try {
			JDBCConnection j = new JDBCConnection();
			jdbcTemplate.setDataSource(j.getDataSourceAuxiliar());
			retornedObj = jdbcTemplate .queryForObject(FIND_BY_ID, params, new UserRowMapper());
		}catch (EmptyResultDataAccessException e) {
			retornedObj = null;
		}catch (Exception e) {
			e.printStackTrace();
		}

		return retornedObj;
	}

	

	@Override
	public User loginUser(String idUser, String password) throws IOException {
		User retornedObj = null;
		Object[] params = { idUser,  password};
		JDBCConnection j = new JDBCConnection();
		jdbcTemplate.setDataSource(j.getDataSourceAuxiliar());
		try {
			retornedObj = jdbcTemplate.queryForObject(LOGIN_USER, params, new UserRowMapper());			
		} catch (EmptyResultDataAccessException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return retornedObj;
	}

	

}
