package com.jbc.oauth2.daoimpl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.jbc.oauth2.dao.UserTypeDao;
import com.jbc.oauth2.mapper.UserTypeRowMapper;
import com.jbc.oauth2.model.UserType;
import com.jbc.oauth2.util.JDBCConnection;
import com.jbc.oauth2.util.UtilDao;



@Repository
public class UserTypeDaoImpl implements UserTypeDao {
	
	JdbcTemplate jdbcTemplate = new JdbcTemplate();
	
	private Properties config = new Properties();

	private static final String FIND_BY_ID;

	static {
		FIND_BY_ID 		= UtilDao.generateQuery("findByIdUserType", 1);
	}

	@Override
	public UserType findByIdUserType(Integer id)  throws IOException {
		UserType retornedObj;
		Object[] params = { id };
		JDBCConnection j = new JDBCConnection();
		jdbcTemplate.setDataSource(j.getDataSourceAuxiliar());
		try {
			retornedObj = jdbcTemplate.queryForObject(FIND_BY_ID, params, new UserTypeRowMapper());
		} catch (EmptyResultDataAccessException e) {
			retornedObj = null;
		}

		return retornedObj;
	}

}
